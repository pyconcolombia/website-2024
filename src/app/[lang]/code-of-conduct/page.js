import React from 'react';

import Title from './components/Title';
import Content from './components/Content';

export const metadata = {
  title: 'Code of Conduct'
};

const codeOfConduct = () => {
  return (
    <>
      <Title />
      <Content />
    </>
  );
};

export default codeOfConduct;
