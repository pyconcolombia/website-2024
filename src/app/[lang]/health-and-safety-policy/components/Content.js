import React from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

const Content = () => {
  return (
    <div className="call-for-proposals">
      <div className="content minor-padding">
        <Container>
          <Row className="justify-content-center">
            <Col xs={12} md={10}>
              <p>
                Our attendees&apos; health and safety remain our top priority as we continue to
                monitor the state of the pandemic and look to venue, local, and CDC guidelines to
                make the best and most informed decisions around onsite safety and requirements.
                Python Colombia has worked hard to be a community that is welcoming to all so we
                will be erring on the side of safety for all participants.
              </p>
              <br />
              <p>
                PyCon Colombia will continue to provide social distancing where possible in the
                venue. The guidelines implemented for PyCon Colombia 2023 are subject to change
                based on health and safety recommendations at the time of the event. We are
                committing, however, to only make changes in the direction of greater protections.
              </p>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};

export default Content;
