import React from 'react';

import Title from './components/Title';
import Content from './components/Content';

export const metadata = {
  title: 'Health and Safety Policy'
};

const healthAndSafetyPolicy = () => {
  return (
    <>
      <Title />
      <Content />
    </>
  );
};

export default healthAndSafetyPolicy;
