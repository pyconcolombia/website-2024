import React from 'react';
import Link from 'next/link';
import propTypes from 'prop-types';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'next/image';

const Card = ({ speakerData, reverse, index, lang }) => {
  const colorBorderSpeaker = ['border-pink', 'border-yellow', 'border-purple', 'border-blue'];

  return (
    <Row className="keynote-card justify-content-center">
      <Col xs={12}>
        <div className="speaker-img-wrapper">
          <Link href={`/${lang}/speakers/${speakerData.id}`}>
            <Image
              className={`img-keynote ${colorBorderSpeaker[(index + 1) % colorBorderSpeaker.length]}`}
              src={`/images/${speakerData.type}/${speakerData.photo}`}
              alt="Speaker Image"
              width={300}
              height={300}
            />
          </Link>
          <div className={`keynote-title ${reverse ? 'text-right' : 'text-left'}`}>
            <span className="bold">
              {speakerData.first_name} {speakerData.last_name}
            </span>
            {speakerData.country_origin && (
              <span className="flag">
                {' '}
                <span className={`fi fi-${speakerData.country_origin}`}></span>
              </span>
            )}
          </div>
          <p>{speakerData.talk.title[lang]}</p>
        </div>
      </Col>
    </Row>
  );
};

Card.propTypes = {
  speakerData: propTypes.shape({
    id: propTypes.string,
    first_name: propTypes.string,
    last_name: propTypes.string,
    biography: propTypes.shape({}),
    photo: propTypes.string,
    type: propTypes.string,
    country_origin: propTypes.string,
    facebook: propTypes.string,
    twitter: propTypes.string,
    linkedin: propTypes.string,
    github: propTypes.string,
    website: propTypes.string
  }),
  reverse: propTypes.bool,
  index: propTypes.number,
  lang: propTypes.string
};

export default Card;
