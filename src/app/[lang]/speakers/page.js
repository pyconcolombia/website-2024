'use server';

import React from 'react';
import propTypes from 'prop-types';
import Speakers from './components/Speakers';

import en from '@/data/dictionaries/en.json';
import es from '@/data/dictionaries/es.json';

export async function generateMetadata({ params: { lang } }, parent) {
  const dataLang = lang === 'en' ? en : es;
  const dataSection = dataLang?.sections;
  const SpeakersData = dataSection.speakers;

  return {
    title: SpeakersData.title
  };
}

const Page = ({ params: { lang } }) => {
  return <Speakers lang={lang} />;
};

Page.propTypes = {
  params: propTypes.shape({
    lang: propTypes.string
  })
};

export default Page;
